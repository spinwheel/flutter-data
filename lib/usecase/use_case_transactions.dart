import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/interface/usecase.dart';
import 'package:sw_core/response/response_transactions.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_data/data/i_repo_spin.dart';

class UseCaseTransactions extends UseCase<ResponseTransactions> {
  UseCaseTransactions(this._repo);

  final IRepoTransaction _repo;

  @override
  Future<ResponseTransactions> execute([dynamic object]) async =>
      update(_repo.getTransactions(get<IUseCaseAuth>().user.value.id));

  @override
  final observable = ResponseTransactions().obs;
}
