import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_core/interface/usecase.dart';
import 'package:sw_data/data/i_repo_spin.dart';

class UseCaseServicer extends UseCase<Servicer> {
  UseCaseServicer(this._repo);

  final IRepoServicer _repo;

  @override
  Future<Servicer> execute([dynamic object]) async =>
      update(_repo.getServicer(object));

  @override
  Rx<Servicer> observable = Servicer().obs;
}
