import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:sw_core/entity_loan_calc/loan_calculated.dart';
import 'package:sw_core/interface/usecase.dart';
import 'package:sw_data/data/i_repo_spin.dart';

class UseCaseLoanCalc extends UseCase<LoanCalculated> {
  UseCaseLoanCalc(this._repo);

  final IRepoLoanCalc _repo;

  @override
  Future<LoanCalculated> execute([dynamic object]) async =>
      update(_repo.requestLoanCalc(object));

  @override
  Rx<LoanCalculated> observable = LoanCalculated().obs;
}
