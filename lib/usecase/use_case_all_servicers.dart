import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:sw_core/usecase/usecase_base.dart';
import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_core/interface/usecase/i_usecase_all_servicers.dart';
import 'package:sw_core/tool/response_handler.dart';
import 'package:sw_data/data/endpoint_all_servicers.dart';
import 'package:sw_data/data/endpoint_servicer.dart';

class UseCaseAllServicers extends UseCaseBase
    with ResponseHandler
    implements IUseCaseAllServicers {
  UseCaseAllServicers(
    this._endAllServicers,
    this._endServicer,
  );

  final EndpointAllServicers _endAllServicers;
  final EndpointServicer _endServicer;

  @override
  RxList<Servicer> allServicers = RxList.empty();

  @override
  Rx<Servicer> servicer = Servicer().obs;

  @override
  Future<List<Servicer>> getAllServicers() async => await _endAllServicers
      .getAllServicers()
      .onError((error, stackTrace) => onResponseError(error))
      .then((response) => allServicers.value = response.allServicers);

  @override
  Future<Servicer> getServicer(String servicerID) async => await _endServicer
      .getServicer(servicerID)
      .onError((error, stackTrace) => onResponseError(error))
      .then((response) => servicer.value = response.servicer);
}
