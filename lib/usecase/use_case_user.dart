// import 'package:get/get_rx/src/rx_types/rx_types.dart';
// import 'package:sw_core/entity_user/user.dart';
// import 'package:sw_core/interface/controller/i_controller_auth.dart';
// import 'package:sw_core/interface/i_partner_auth.dart';
// import 'package:sw_core/interface/usecase.dart';
// import 'package:sw_core/tool/inject.dart';
// import 'package:sw_data/data/i_repo_spin.dart';
//
// class UseCaseUser extends UseCase<User> {
//   UseCaseUser(this._repo);
//
//   final IRepoUser _repo;
//
//   @override
//   Rx<User> observable = User().obs;
//
//   @override
//   Future<User> execute([dynamic object]) async =>
//       update(_repo.getUser(get<IControllerAuth>().user.value.id));
// }
