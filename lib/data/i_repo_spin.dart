import 'package:sw_core/entity_loan_calc/loan_calculated.dart';
import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_core/entity_user/user.dart';
import 'package:sw_core/request/request_auth.dart';
import 'package:sw_core/request/request_auth_add_steps.dart';
import 'package:sw_core/request/request_loan_calc.dart';
import 'package:sw_core/response/response_all_servicers.dart';
import 'package:sw_core/response/response_transactions.dart';

abstract class IRepoSpin
    implements IRepoAuth, IRepoLoanCalc, IRepoServicer, IRepoTransaction, IRepoUser {}

abstract class IRepoAuth {
  Future<User> requestAuth(RequestAuth authentication);

  Future<User> requestAuthAddSteps(RequestAuthAddSteps authentication);
}

abstract class IRepoLoanCalc {
  Future<LoanCalculated> requestLoanCalc(RequestLoanCalc loan);
}

abstract class IRepoUser {
  Future<User> getUser(String userID);
}

abstract class IRepoServicer {
  Future<Servicer> getServicer(String servicerID);
}

abstract class IRepoAllServicers {
  Future<ResponseAllServicers> getAllServicers();
}

abstract class IRepoTransaction {
  Future<ResponseTransactions> getTransactions(String userID);
}
