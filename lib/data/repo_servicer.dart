import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_data/data/endpoint_servicer.dart';
import 'package:sw_data/data/repo_base.dart';

import 'i_repo_spin.dart';

class RepoServicer extends RepoBase implements IRepoServicer {
  RepoServicer(this.endpoint);

  final EndpointServicer endpoint;

  @override
  Future<Servicer> getServicer(String servicerID) async =>
      (await handle(endpoint.getServicer(servicerID))).servicer;
}
