import 'package:dio/dio.dart';
import 'package:sw_core/tool/log.dart';

class RepoBase {
  Future<T> handle<T>(Future<T> response) {
    response.then(_onValue).catchError(_onError);
    return response;
  }

  _onValue<T>(T value) => info(value);

  _onError(Object object) {
    switch (object.runtimeType) {
      case DioError:
        final errorResponse = (object as DioError).response;
        error("Got error : ${errorResponse?.statusCode} -> ${errorResponse?.statusMessage}");
        break;
      default:
    }
  }
}
