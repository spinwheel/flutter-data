// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'endpoint_servicer.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _EndpointServicer implements EndpointServicer {
  _EndpointServicer(this._dio, {this.baseUrl}) {
    baseUrl ??= get<SWEnvironment>().url;
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ResponseServicer> getServicer(servicerID) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ResponseServicer>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/v1/md/loanServicers/$servicerID',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ResponseServicer.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
