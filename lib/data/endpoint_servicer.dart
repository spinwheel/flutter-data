import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:sw_core/environment/sw_env.dart';
import 'package:sw_core/response/response_servicer.dart';
import 'package:sw_core/tool/data_path.dart';
import 'package:sw_core/tool/inject.dart';

part 'endpoint_servicer.g.dart';

@RestApi(baseUrl: base_SW_URL)
abstract class EndpointServicer {
  factory EndpointServicer(Dio dio, {String baseUrl}) = _EndpointServicer;

  @GET("$servicer{$servicerID}")
  Future<ResponseServicer> getServicer(
    @Path(servicerID) String servicerID,
  );
}
