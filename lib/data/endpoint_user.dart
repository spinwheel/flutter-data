import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:sw_core/environment/sw_env.dart';
import 'package:sw_core/response/response_auth.dart';
import 'package:sw_core/tool/data_path.dart';
import 'package:sw_core/tool/inject.dart';

part 'endpoint_user.g.dart';

@RestApi(baseUrl: base_SW_URL)
abstract class EndpointUser {
  factory EndpointUser(Dio dio, {String baseUrl}) = _EndpointUser;

  @GET(user)
  Future<ResponseAuth> getUser(
    @Query(extUserId) String extUserId,
  );
}
