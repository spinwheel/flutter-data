import 'package:sw_core/response/response_transactions.dart';
import 'package:sw_data/data/endpoint_transactions.dart';
import 'package:sw_data/data/repo_base.dart';

import 'i_repo_spin.dart';

class RepoTransactions extends RepoBase implements IRepoTransaction {
  RepoTransactions(this.endpoint);

  final EndpointTransactions endpoint;

  @override
  Future<ResponseTransactions> getTransactions(String userID) async =>
      (await handle(endpoint.getTransactions(userID)));
}
