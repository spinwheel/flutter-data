import 'package:sw_core/response/response_all_servicers.dart';
import 'package:sw_data/data/endpoint_all_servicers.dart';
import 'package:sw_data/data/repo_base.dart';

import 'i_repo_spin.dart';

class RepoAllServicers extends RepoBase implements IRepoAllServicers {
  RepoAllServicers(this.endpoint);

  final EndpointAllServicers endpoint;

  @override
  Future<ResponseAllServicers> getAllServicers() async =>
      (await handle(endpoint.getAllServicers()));
}
