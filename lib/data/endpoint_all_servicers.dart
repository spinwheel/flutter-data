import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:sw_core/environment/sw_env.dart';
import 'package:sw_core/response/response_all_servicers.dart';
import 'package:sw_core/tool/data_path.dart';
import 'package:sw_core/tool/inject.dart';

part 'endpoint_all_servicers.g.dart';

@RestApi(baseUrl: base_SW_URL)
abstract class EndpointAllServicers {
  factory EndpointAllServicers(Dio dio, {String baseUrl}) = _EndpointAllServicers;

  @GET(servicer)
  Future<ResponseAllServicers> getAllServicers();
}
