import 'package:sw_core/entity_loan_calc/loan_calculated.dart';
import 'package:sw_core/request/request_loan_calc.dart';
import 'package:sw_data/data/endpoint_loan_calc.dart';
import 'package:sw_data/data/repo_base.dart';

import 'i_repo_spin.dart';

class RepoLoanCalc extends RepoBase implements IRepoLoanCalc {
  RepoLoanCalc(this.endpoint);

  final EndpointLoanCalc endpoint;

  @override
  Future<LoanCalculated> requestLoanCalc(RequestLoanCalc loan) async =>
      (await handle(endpoint.getLoanCalc(loan))).loanCalc!;
}
