import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:sw_core/environment/sw_env.dart';
import 'package:sw_core/response/response_transactions.dart';
import 'package:sw_core/tool/data_path.dart';
import 'package:sw_core/tool/inject.dart';

part 'endpoint_transactions.g.dart';

@RestApi(baseUrl: base_SW_URL)
abstract class EndpointTransactions {
  factory EndpointTransactions(Dio dio, {String baseUrl}) = _EndpointTransactions;

  @GET("$user{$userID}$transactions")
  Future<ResponseTransactions> getTransactions(
    @Path(userID) String userID,
  );
}
