import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:sw_core/environment/sw_env.dart';
import 'package:sw_core/request/request_loan_calc.dart';
import 'package:sw_core/response/response_loan_calculator.dart';
import 'package:sw_core/tool/data_path.dart';
import 'package:sw_core/tool/inject.dart';

part 'endpoint_loan_calc.g.dart';

@RestApi(baseUrl: base_SW_URL)
abstract class EndpointLoanCalc {
  factory EndpointLoanCalc(Dio dio, {String baseUrl}) = _EndpointLoanCalc;

  @POST(calculator)
  Future<ResponseLoanCalculator> getLoanCalc(
    @Body() RequestLoanCalc loan,
  );
}
