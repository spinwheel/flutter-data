import 'package:dio/dio.dart';
import 'package:sw_core/sw_init_core.dart';
import 'package:sw_core/tool/http_client.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_data/data/endpoint_loan_calc.dart';
import 'package:sw_data/data/endpoint_servicer.dart';
import 'package:sw_data/data/endpoint_transactions.dart';
import 'package:sw_data/data/repo_loan_calc.dart';
import 'package:sw_data/data/repo_servicer.dart';
import 'package:sw_data/data/repo_transaction.dart';
import 'package:sw_data/usecase/use_case_all_servicers.dart';
import 'package:sw_data/usecase/use_case_loan_calculated.dart';
import 'package:sw_data/usecase/use_case_servicer.dart';
import 'package:sw_data/usecase/use_case_transactions.dart';

import 'data/endpoint_all_servicers.dart';

initData() async {
  await initCore();
  await _initUseCases();
}

_initUseCases() async {
  put<UseCaseAllServicers>(
    () => UseCaseAllServicers(
      EndpointAllServicers(
        httpClient(
          [
            httpCache(60),
            get<InterceptorsWrapper>(),
          ],
        ),
      ),
      EndpointServicer(
        httpClient(
          [
            httpCache(60),
            get<InterceptorsWrapper>(),
          ],
        ),
      ),
    ),
  );
  put<UseCaseServicer>(
    () => UseCaseServicer(
      RepoServicer(
        EndpointServicer(
          httpClient(
            [
              httpInterceptors(),
            ],
          ),
        ),
      ),
    ),
  );
  put<UseCaseTransactions>(
    () => UseCaseTransactions(
      RepoTransactions(
        EndpointTransactions(
          httpClient(
            [
              httpInterceptors(),
            ],
          ),
        ),
      ),
    ),
  );
  put<UseCaseLoanCalc>(
    () => UseCaseLoanCalc(
      RepoLoanCalc(
        EndpointLoanCalc(
          httpClient(
            [
              httpInterceptors(),
            ],
          ),
        ),
      ),
    ),
  );
}
