@echo off

echo ________________________________      DELETE PUBSPEC.LOCK DATA
call del "pubspec.lock"
echo ________________________________      FLUTTER CLEAN DATA
call flutter clean
echo ________________________________      FLUTTER PUB UPGRADE DATA
call flutter pub upgrade
echo ________________________________      FLUTTER PUB GET DATA
call flutter pub get